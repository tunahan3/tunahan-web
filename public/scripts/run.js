$(document).ready(function () {
    $("a[rel~=external]").attr("target", "_blank");
    $('.construction-giris').click(function () {
        $('.construction-category').css({'display': 'block'});
    });

    kvkkRead();

    loginIsCheckedControl();
});

function loginIsCheckedControl() {


    var nms = 0;
    $('.form-check-input').each(function (index) {
        if($(this).prop("checked")){
            nms++;
        }else{
            nms--;
        }
    });
    if(nms >= 2){
        //$('.btn-send-cntr').removeClass('hide');
        $('.btn-send-cntr').removeAttr('disabled');
    }else{
        // $('.btn-send-cntr').addClass('hide');
        $('.btn-send-cntr').attr('disabled', 'disabled');
    }


    var nms2 = 0;
    $('.form-check-input').change(function () {
        if($(this).prop("checked")){
            nms2++;
        }else{
            nms2--;
        }
        if(nms2 >= 2){
            // $('.btn-send-cntr').removeClass('hide');
            $('.btn-send-cntr').removeAttr('disabled');
        }else{
            // $('.btn-send-cntr').addClass('hide');
            $('.btn-send-cntr').attr('disabled', 'disabled');
        }
    });
}

function setCookie(c_name,value,exdays){
    cookie_domain = '';
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+exdays);
    var c_value=escape(value)+((exdays==null)?'':';'+cookie_domain+';Path=/;SameSite=Lax;expires='+exdate.toUTCString());
    document.cookie=c_name+'='+c_value}

function getCookie(c_name){
    var i,x,y,ARRcookies=document.cookie.split(';');
    for(i=0;i<ARRcookies.length;i++){
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf('='));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf('=')+1);
        x=x.replace(/^\s+|\s+$/g,'');
        if(x==c_name){return unescape(y)}
    }
}
function kvkk() {
    setCookie("kvkk","true",365);
    $(".footer-kvkk").addClass('hide');
}
function kvkkRead(){
    var kvkk = getCookie("kvkk");
    if (kvkk == "true"){
        $(".footer-kvkk").addClass('hide');
    }else{
        $(".footer-kvkk").removeClass('hide');
    }
}

function openURL(url) {
    window.location.href = url;
}

function recommend(url) {
    var disp_setting = "toolbar=no,location=no,directories=no,menubar=no,";
    disp_setting += "scrollbars=yes,width=640, height=480, left=100, top=25";
    var docprint = window.open(url, "send_mail", disp_setting);
    docprint.document.close();
    docprint.focus();
    return false;
}

function onlyNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        return true;
    }
}

function accordAnim() {
    $('#accordion h3').bind('click', function () {
        var newIndex = $(this).index('#accordion h3');
        if ($(this).parent().hasClass('active')) {
            $(this).parent().find('.accord-content').slideUp("slow", function () {
                $('#accordion .accord-border').removeClass('active');
            });
        } else {
            $('#accordion .accord-border .accord-content').slideUp("slow", function () {
                $("#accordion .accord-border").removeClass('active');
            });
            $('#accordion .accord-border .accord-content:eq(' + newIndex + ')').slideDown("slow", function () {
                $("#accordion .accord-border").eq(newIndex).addClass("active");
            });
        }
        return false;
    });
    /*$('#accordion .accord-content:eq(0)').slideDown("slow");*/
}

function yukariAssagi() {
    $('.yukariAssagi').click(function () {
        // height: 316px; bottom: 0px;
        if ($('.yukariAssagi').hasClass('active')) {
            $(this).removeClass('active');
            $('.prj-yaylan').animate({
                bottom: "-266"
            }, 2000);
            $('.prj-wrappers').animate({
                height: "64"
            }, 2000);
        } else {
            $(this).addClass('active');
            $('.prj-yaylan').animate({
                bottom: "0"
            }, 2000);
            $('.prj-wrappers').animate({
                height: "330"
            }, 2000);
        }
    });
    $('#headerOpenClose>.menuOC').click(function () {
        // height: 316px; bottom: 0px;
        if ($('#headerOpenClose>.menuOC').hasClass('active')) {
            $(this).removeClass('active');
            $('#headerArea').animate({
                top: "-60"
            }, 2000);
        } else {
            $(this).addClass('active');
            $('#headerArea').animate({
                top: "0"
            }, 2000);
        }
    });
    $("#projectsPausePlay").click(function () {
        api.playToggle();
    });
}

function galeriNextBack() {
    $('.superGallery li').click(function () {
        index = $(this).index();
        api.goTo(index + 1);
        return false;
    });
}


