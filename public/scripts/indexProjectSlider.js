var totalSlideP = 0;
var B_Timer = null;
var B_Sure = 5000; /* 5 Saniye */ 
var slideP = 0;
$(document).ready(function(){	
    totalSlideP = $(".indexProje-1 img").length;
    animasyon(0);    
    B_Timer = setInterval('timerFonks()', B_Sure);
});
function timerFonks(){ 
    if(slideP == (totalSlideP-1) ){			
        slideP = 0;
    }else{
        slideP++;
    }   		
    animasyon(slideP);
}
function animasyon(gelen){    
    $(".indexProje-1").children("img").fadeOut(800);    
    $(".indexProje-1").children("img").eq(gelen).fadeIn(900);
    $(".indexProje-2").children("img").fadeOut(800);    
    $(".indexProje-2").children("img").eq(gelen).fadeIn(900);
    $(".indexProje-3").children("img").fadeOut(800);    
    $(".indexProje-3").children("img").eq(gelen).fadeIn(900);
}