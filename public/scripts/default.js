var locs = new Array();
/*
 locs['home'] = 0;
 locs['profil'] = 1080;
 locs['proje'] = 2160;
 locs['uygulama'] = 3240;
 locs['ekibimiz'] = 4320;
 locs['iletisim'] = 5400;
 */
locs['home'] = 0;
locs['profil'] = 920;
locs['proje'] = 1840;
locs['uygulama'] = 2760;
locs['hakkimizda'] = 3680;
locs['iletisim'] = 4600;

//
// browser
isMobileBrowser = false;


$(document).ready(function () {

    //
    isMobileBrowser = _setBrowser();
    // isMobileBrowser = true;
    console.log(isMobileBrowser);


    if (isMobileBrowser == false) {

        htmlbody = $('html,body');
        s = skrollr.init({
            forceHeight: true,
            smoothScrolling: true,
            smoothScrollingDuration: 1000,
            easing: 'swing'
        });

    }
    /*
     //The options (second parameter) are all optional. The values shown are the default values.
     skrollr.menu.init(s, {
     //skrollr will smoothly animate to the new position using `animateTo`.
     animate: false,
     //The easing function to use.
     easing: 'sqrt',
     //How long the animation should take in ms.
     duration: function(currentTop, targetTop) {
     //By default, the duration is hardcoded at 500ms.
     return 1500;

     //But you could calculate a value based on the current scroll position (`currentTop`) and the target scroll position (`targetTop`).
     //return Math.abs(currentTop - targetTop) * 10;
     },
     });
     */


    $('.button').click(function (e) {
        e.preventDefault();
        dataslide = $(this).attr('href');
        dataslide = dataslide.replace('#', '');
        goToByScroll(dataslide);

    });
    function goToByScroll(dataslide) {
        if (isMobileBrowser == true) {
            window.location.hash = '#' + dataslide;
        } else {

            jQuery('html:not(:animated), body:not(:animated)').animate({
                scrollTop: locs[dataslide]
            }, 1500, function () {
                window.location.hash = '#' + dataslide;
            });

        }

    }

    if (isMobileBrowser == false) {
        domHeightWidth(s);
    }else{
        domHeightWidthMobile();
    }
    sirketProfili();

    iletisimFormu();


});
$(window).on("scroll", function () {
    position = $(this).scrollTop();
    $('.infoPos').html(position);
});
$(window).resize(function () {
    if (isMobileBrowser == false) {
        domHeightWidth(s);
    }else{
        domHeightWidthMobile();
    }
});
function domHeightWidth(skroll) {
    setVideoSize();
    skroll.refresh();
}
function domHeightWidthMobile() {
    setVideoSize();
}
function setVideoSize() {
    $('.videoPlay').css({width: $(window).width()});
}
/*
 function setHeightWidth()
 {
 var windowHeight = $(window).height();
 var windowWidth = $(window).width();
 $('.full-height').css({height: windowHeight});
 videoPlay
 }
 */

$(function () {
});
function sirketProfili() {
    $('#profil ul.menus li').on('click', function () {
        var ind = $(this).index();

        $(".textTasiyici>.back").css({'display': 'block'});
        $("#profil .text .textinfo").removeClass("active");
        $("#profil ul.menus li").removeClass("active");

        $("#profil .text .textinfo").eq(ind).addClass("active");
        $("#profil ul.menus li").eq(ind).addClass("active");
    });
    $('.textTasiyici>.back>span').on('click', function () {
        var ind = 0;

        $(".textTasiyici>.back").css({'display': 'none'});
        $("#profil .text .textinfo").removeClass("active");
        $("#profil ul.menus li").removeClass("active");

        $("#profil .text .textinfo").eq(ind).addClass("active");
        $("#profil ul.menus li").eq(ind).addClass("active");
    });
    return false;
    //return menusl
}

$.fn.onLoadPop = function (url) {
    $(window).load(function () {
        $.magnificPopup.open({
            settings: null,
            cursor: 'mfp-ajax-cur',
            mainClass   : 'mfp-fade',
            items: {
                src : '/pop-up.php' + url,
                type: 'iframe',
                alignTop: true
            }
        });
        //setInterval("close()", 17000);
    });
};

function iletisimFormu() {
    $("#contactForm input[type='button']").click(function () {
        var sonuc = "";

        var _realName = $("#_realname").val();
        var _email = $("#_email").val();
        var _token = $("#token").val();

        var adiSoyadi = $("#adiSoyadi").val();
        /* * */
        var email = $("#email").val();
        /* * */
        var telephone = $("#telephone").val();
        /* * */
        var fax = $("#fax").val();
        var address = $("#address").val();
        var topic = $("#topic").val();
        var message = $("#message").val();
        /* * */

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(email) == false) {
            sonuc += "Lütfen E-Posta adresinizi kontrol ediniz.";
            $('.iletisimSonuc').empty();
            $('.iletisimSonuc').append(sonuc);
            $('.iletisimSonuc').css('display', 'block');
            return false;
        } else {
            if (adiSoyadi == '' || email == '' || telephone == '' || message == '') {
                sonuc += "Lütfen formu doldurunuz...";
                $('.iletisimSonuc').empty();
                $('.iletisimSonuc').append(sonuc);
                $('.iletisimSonuc').css('display', 'block');
                return false;
            } else {
                //-----------------------------
                var data = '&adiSoyadi=' + adiSoyadi + '&email=' + email + '&telephone=' + telephone + '&fax=' + fax + '&address=' + address + '&topic=' + topic + '&message=' + message;
                var request = $.ajax({
                    url: "/iletisim-kayit.php",
                    type: "POST",
                    data: data,
                    cache: false,
                    dataType: "html"
                });
                request.done(function (html) {
                    sonuc += "" + html + " ";
                    $('.iletisimSonuc').empty();
                    $('.iletisimSonuc').append(sonuc);
                    $('.iletisimSonuc').css('display', 'block');
                    //-----------------------------
                    //$("#adiSoyadi").val('');
                    //$("#email").val('');
                    //$("#telephone").val('');
                    //$("#fax").val('');
                    //$("#address").val('');
                    //$("#topic").val('');
                    //$("#message").val('');
                    //-----------------------------
                    return false;

                });
                request.fail(function (jqXHR, textStatus) {
                    sonuc += "Request failed: " + textStatus;
                    $('.iletisimSonuc').empty();
                    $('.iletisimSonuc').append(sonuc);
                    $('.iletisimSonuc').css('display', 'block');
                    return false;
                });
                return false;
                //-----------------------------
            }


        }
        $('.yorumSonuc').empty();
        $('.yorumSonuc').append(sonuc);
        $('.yorumSonuc').css('display', 'block');
        hidem('.yorumSonuc');
        return false;
    });
}