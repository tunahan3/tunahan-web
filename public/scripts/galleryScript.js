var totalSlide = 0;
var B_Timer = null;
var B_Sure = 5000;
/* 5 Saniye */
var slide = 0;
$(document).ready(function () {
    totalSlide = $("#consGallery>span").length;
    animasyon(0);
    if (totalSlide > 1) {
        B_Timer = setInterval('timerFonks()', B_Sure);

        $('#consGalleryArea>.prew').click(function () {
            slide = (slide - 1);
            if (slide < 0) {
                slide = (totalSlide - 1);
            }
            clearInterval(B_Timer);
            setTimeout('timerPrewNextFonks()', 0);
            B_Timer = setInterval('timerFonks()', B_Sure);
        });
        $('#consGalleryArea>.next').click(function () {
            slide = (slide + 1);
            if (slide == (totalSlide)) {
                slide = 0;
            }
            clearInterval(B_Timer);
            setTimeout('timerPrewNextFonks()', 0);
            B_Timer = setInterval('timerFonks()', B_Sure);
        });

        $('#consGallery').mouseover(function () {
            clearInterval(B_Timer);
        });
        $('#consGallery').mouseout(function () {
            B_Timer = setInterval('timerFonks()', B_Sure);
        });
    }
});
function timerPrewNextFonks() {
    animasyon(slide);
}
function timerFonks() {
    if (slide == (totalSlide - 1)) {
        slide = 0;
    } else {
        slide++;
    }
    animasyon(slide);
}
function animasyon(gelen) {
    $("#consGallery>span").fadeOut(900)
    $("#consGallery>span").eq(gelen).fadeIn(1000);
}